import 'package:flutter/material.dart';
import 'package:wheather_app/FirstScreen.dart';

class SecondScreen extends StatefulWidget {
  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>FirstScreen(countryName: "America",)));
          },
          child: Text("Go Back"),
        ),
      ),
    );
  }
}
