import 'dart:math';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import 'SecondScreen.dart';
import 'WeatherModel.dart';
import 'WeatherService.dart';

class FirstScreen extends StatefulWidget {
  final countryName;

  const FirstScreen({Key key, this.countryName}) : super(key: key);

  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  WeatherModel weatherModel = WeatherModel();
  Position position = Position();
  String place;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    if(widget.countryName!=null){
      fetchDetails(widget.countryName);
    }
    else{
      _getCurrentLocation();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : SafeArea(
            child: Column(
                children: <Widget>[
                  Text(weatherModel.id.toString()),
                  RaisedButton(
                    child: Text("To Next Screen"),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SecondScreen()));
                    },
                  )
                ],
              ),
          ),
    );
  }

  fetchDetails(String locality) {
    WeatherService.getWeatherData(locality).then((result) {
      if (result != null) {
        weatherModel = result;
        if (weatherModel != null) {
          setState(() {
            isLoading = false;
          });
        }
      }
    });
  }

  _getCurrentLocation() async {
    try {
      Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
          .then((result) {
        position = result;
        print(position);
        if (position != null) {
          _getAddressFromLatLng();
        }
      });
    } catch (e) {
      print(e.toString() + "error");
    }
  }

  _getAddressFromLatLng() async {
    try {
      Geolocator()
          .placemarkFromCoordinates(position.latitude, position.longitude)
          .then((result) {
        place = result[0].name.split(" ")[0];
        print(place);
        if (place != null) {
          fetchDetails(place);
        }
      });
    } catch (e) {
      print(e.toString() + "error");
    }
  }
}
