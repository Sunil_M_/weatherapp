import 'WeatherModel.dart';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class WeatherService {
  static Future<WeatherModel> getWeatherData(String locality) async {
    String url =
        "http://api.openweathermap.org/data/2.5/weather?q=$locality&appid=36afa4f15510f60b0ebcbde444ce43a8";
    try {
      var response = await http.get(url);

      if (response.statusCode == 200) {
        WeatherModel weatherModel =
            WeatherModel.fromJson(json.decode(response.body));
        if (weatherModel != null) {
          print("Weather response");
          print(weatherModel.toJson());
          return weatherModel;
        } else {
          return null;
        }
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
      return null;
    }
    return null;
  }
}
